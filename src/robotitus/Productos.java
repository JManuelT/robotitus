/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotitus;

/**
 *
 * @author josemanueltirado
 */
public class Productos {
    
    public String IdProducto, nombreProduc, marca, proveedor;
    public int CostoCompra;
    
    public Productos(){
        
    }

    public Productos(String IdProducto, String nombreProduc, String marca, String proveedor, int CostoCompra) {
        this.IdProducto = IdProducto;
        this.nombreProduc = nombreProduc;
        this.marca = marca;
        this.proveedor = proveedor;
        this.CostoCompra = CostoCompra;
    }

    public String getIdProducto() {
        return IdProducto;
    }

    public String getNombreProduc() {
        return nombreProduc;
    }

    public String getMarca() {
        return marca;
    }

    public String getProveedor() {
        return proveedor;
    }

    public int getCostoCompra() {
        return CostoCompra;
    }

    public void setIdProducto(String IdProducto) {
        this.IdProducto = IdProducto;
    }

    public void setNombreProduc(String nombreProduc) {
        this.nombreProduc = nombreProduc;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public void setCostoCompra(int CostoCompra) {
        this.CostoCompra = CostoCompra;
    }

    @Override
    public String toString() {
        return "Productos{" + "IdProducto=" + IdProducto + ", nombreProduc=" + nombreProduc + ", marca=" + marca + ", proveedor=" + proveedor + ", CostoCompra=" + CostoCompra + '}';
    }
}
