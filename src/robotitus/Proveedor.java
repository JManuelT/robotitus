/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotitus;

/**
 *
 * @author josemanueltirado
 */
public class Proveedor {
    
    public String idProveedor, nombreProveedor, correoE, numeroTel, direccion;
    
    public Proveedor(){
        
    }

    public Proveedor(String idProveedor, String nombreProveedor, String correoE, String numeroTel, String direccion) {
        this.idProveedor = idProveedor;
        this.nombreProveedor = nombreProveedor;
        this.correoE = correoE;
        this.numeroTel = numeroTel;
        this.direccion = direccion;
    }

    public String getIdProveedor() {
        return idProveedor;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public String getCorreoE() {
        return correoE;
    }

    public String getNumeroTel() {
        return numeroTel;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setIdProveedor(String idProveedor) {
        this.idProveedor = idProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    public void setCorreoE(String correoE) {
        this.correoE = correoE;
    }

    public void setNumeroTel(String numeroTel) {
        this.numeroTel = numeroTel;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "Proveedor{" + "idProveedor=" + idProveedor + ", nombreProveedor=" + nombreProveedor + ", correoE=" + correoE + ", numeroTel=" + numeroTel + ", direccion=" + direccion + '}';
    }
}
