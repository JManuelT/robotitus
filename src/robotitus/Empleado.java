/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotitus;

/**
 *
 * @author josemanueltirado
 */
public class Empleado {
     public String nombreEmpleado,IdEmpleado, antiguedad, numeroTel;
    public int edad, salario;
    
    public Empleado(){
    }

    public Empleado(String nombreEmpleado, String IdEmpleado, String antiguedad, String numeroTel, int edad, int salario) {
        this.nombreEmpleado = nombreEmpleado;
        this.IdEmpleado = IdEmpleado;
        this.antiguedad = antiguedad;
        this.numeroTel = numeroTel;
        this.edad = edad;
        this.salario = salario;
    }

    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    public String getIdEmpleado() {
        return IdEmpleado;
    }

    public String getAntiguedad() {
        return antiguedad;
    }

    public String getNumeroTel() {
        return numeroTel;
    }

    public int getEdad() {
        return edad;
    }

    public int getSalario() {
        return salario;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    public void setIdEmpleado(String IdEmpleado) {
        this.IdEmpleado = IdEmpleado;
    }

    public void setAntiguedad(String antiguedad) {
        this.antiguedad = antiguedad;
    }

    public void setNumeroTel(String numeroTel) {
        this.numeroTel = numeroTel;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    @Override
    public String toString() {
        return "Empleado{" + "nombreEmpleado=" + nombreEmpleado + ", IdEmpleado=" + IdEmpleado + ", antiguedad=" + antiguedad + ", numeroTel=" + numeroTel + ", edad=" + edad + ", salario=" + salario + '}';
    }
}
