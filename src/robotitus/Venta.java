/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotitus;

/**
 *
 * @author josemanueltirado
 */
public class Venta {
     public String idVenta, fechaHoraVenta, fkEmpleado, fkCliente, fkProducto;
    public int valorVenta;
    
    public Venta(){
        
    }

    public Venta(String idVenta, String fechaHoraVenta, String fkEmpleado, 
            String fkCliente, String fkProducto, int valorVenta) {
        this.idVenta = idVenta;
        this.fechaHoraVenta = fechaHoraVenta;
        this.fkEmpleado = fkEmpleado;
        this.fkCliente = fkCliente;
        this.fkProducto = fkProducto;
        this.valorVenta = valorVenta;
    }

    public String getIdVenta() {
        return idVenta;
    }

    public String getFechaHoraVenta() {
        return fechaHoraVenta;
    }

    public String getFkEmpleado() {
        return fkEmpleado;
    }

    public String getFkCliente() {
        return fkCliente;
    }

    public String getFkProducto() {
        return fkProducto;
    }

    public int getValorVenta() {
        return valorVenta;
    }

    public void setIdVenta(String idVenta) {
        this.idVenta = idVenta;
    }

    public void setFechaHoraVenta(String fechaHoraVenta) {
        this.fechaHoraVenta = fechaHoraVenta;
    }

    public void setFkEmpleado(String fkEmpleado) {
        this.fkEmpleado = fkEmpleado;
    }

    public void setFkCliente(String fkCliente) {
        this.fkCliente = fkCliente;
    }

    public void setFkProducto(String fkProducto) {
        this.fkProducto = fkProducto;
    }

    public void setValorVenta(int valorVenta) {
        this.valorVenta = valorVenta;
    }

    @Override
    public String toString() {
        return "Ventas{" + "idVenta=" + idVenta + ", fechaHoraVenta=" + 
                fechaHoraVenta + ", fkEmpleado=" + fkEmpleado + ", fkCliente=" +
                fkCliente + ", fkProducto=" + fkProducto + ", valorVenta=" 
                + valorVenta + '}';
    }
}
