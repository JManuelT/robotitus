/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotitus;

/**
 *
 * @author josemanueltirado
 */
public class Cliente {
    
    public int IdCliente,edad;
    public String nombre, metodoPago, dirección;
    

    public Cliente() {

    }

    public Cliente(int Id, int edad, String nombre, String metodoPago, String dirección) {
        this.IdCliente = Id;
        this.edad = edad;
        this.nombre = nombre;
        this.metodoPago = metodoPago;
        this.dirección = dirección;
    }

    public int getId() {
        return IdCliente;
    }

    public int getEdad() {
        return edad;
    }

    public String getNombre() {
        return nombre;
    }

    public String getMetodoPago() {
        return metodoPago;
    }

    public String getDirección() {
        return dirección;
    }

    public void setId(int Id) {
        this.IdCliente = Id;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setMetodoPago(String metodoPago) {
        this.metodoPago = metodoPago;
    }

    public void setDirección(String dirección) {
        this.dirección = dirección;
    }

    @Override
    public String toString() {
        return "Cliente{" + "Id=" + IdCliente + ", edad=" + edad + ", nombre=" +
                nombre + ", metodoPago=" + metodoPago + ", direcci\u00f3n=" + dirección + '}';
    }
    
}